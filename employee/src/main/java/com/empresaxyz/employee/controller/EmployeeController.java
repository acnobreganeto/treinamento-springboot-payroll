package com.empresaxyz.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresaxyz.employee.model.Employee;
import com.empresaxyz.employee.service.EmployeeService;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @GetMapping("/status")
    public String status() {
        return "I am up!";
    }

    @PostMapping("/employee")
    public ResponseEntity<Employee> insert(@RequestBody Employee employee) {
        Employee result = service.insert(employee);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> findAll() {
        List<Employee> result = service.findAll();
        return ResponseEntity.ok(result);
    }

    @PutMapping("/employee")
    public ResponseEntity<Employee> update(@RequestBody Employee employee) {
        Employee result = service.update(employee);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id) {
        service.delete(id);
        return ResponseEntity.ok("Employee deleted!");
    }
}
