package com.empresaxyz.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empresaxyz.employee.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
