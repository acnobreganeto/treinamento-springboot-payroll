package com.empresaxyz.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutesConfig {
    @Bean
    public RouteLocator gateWayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/employee/**").uri("lb://SERVICE-EMPLOYEE"))
                .route(r -> r.path("/invoice/**").uri("lb://SERVICE-INVOICE"))
                .build();
    }
}
