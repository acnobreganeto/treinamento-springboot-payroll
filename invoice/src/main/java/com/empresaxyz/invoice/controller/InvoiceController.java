package com.empresaxyz.invoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empresaxyz.invoice.dto.InvoiceDto;
import com.empresaxyz.invoice.service.InvoiceService;

@RestController
public class InvoiceController {
    @Autowired
    private InvoiceService service;

    @GetMapping("/status")
    public String status() {
        return "I am up!";
    }

    @GetMapping("/invoice")
    public ResponseEntity<InvoiceDto> generateInvoice() {
        InvoiceDto invoice = service.generateInvoice();
        return ResponseEntity.ok(invoice);
    }
}
