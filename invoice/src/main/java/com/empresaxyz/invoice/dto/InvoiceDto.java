package com.empresaxyz.invoice.dto;

import java.time.LocalDateTime;
import java.util.List;

public class InvoiceDto {
    private LocalDateTime dateTime;
    private List<PayslipDto> payslips;

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public List<PayslipDto> getPayslips() {
        return payslips;
    }

    public void setPayslips(List<PayslipDto> payslips) {
        this.payslips = payslips;
    }
}