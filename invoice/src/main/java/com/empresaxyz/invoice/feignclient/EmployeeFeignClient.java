package com.empresaxyz.invoice.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "service-employee")
public interface EmployeeFeignClient {
    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> findAll();
}