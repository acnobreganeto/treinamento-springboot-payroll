package com.empresaxyz.invoice.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.empresaxyz.invoice.dto.EmployeeDto;
import com.empresaxyz.invoice.dto.PayslipDto;

@FeignClient(name = "service-payslip")
public interface PayslipFeignClient {
    @GetMapping("/payslip")
    public ResponseEntity<List<PayslipDto>> payslips(@RequestBody List<EmployeeDto> employees);
}