package com.empresaxyz.invoice.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empresaxyz.invoice.dto.EmployeeDto;
import com.empresaxyz.invoice.dto.InvoiceDto;
import com.empresaxyz.invoice.dto.PayslipDto;
import com.empresaxyz.invoice.feignclient.Employee;
import com.empresaxyz.invoice.feignclient.EmployeeFeignClient;
import com.empresaxyz.invoice.feignclient.PayslipFeignClient;

@Service
public class InvoiceService {
    @Autowired
    private EmployeeFeignClient employee;

    @Autowired
    private PayslipFeignClient payslip;

    public InvoiceDto generateInvoice() {
        List<Employee> employees = employee.findAll().getBody();

        List<EmployeeDto> employeeDtos = new ArrayList<>();

        for (Employee employee : employees) {
            EmployeeDto e = new EmployeeDto();
            e.setId(employee.getId());
            e.setName(employee.getName());
            e.setSalary(employee.getSalary());
            employeeDtos.add(e);
        }

        List<PayslipDto> payslips = payslip.payslips(employeeDtos).getBody();

        InvoiceDto invoice = new InvoiceDto();
        invoice.setDateTime(LocalDateTime.now());
        invoice.setPayslips(payslips);

        return invoice;
    }
}
