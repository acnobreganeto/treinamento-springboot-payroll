package com.empresaxyz.payslip.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresaxyz.payslip.controller.dto.EmployeeDto;
import com.empresaxyz.payslip.controller.dto.PayslipDto;
import com.empresaxyz.payslip.service.PayslipService;

@RestController
public class PayslipController {
    @Autowired
    private PayslipService service;

    @GetMapping("/status")
    public String status() {
        return "I am up!";
    }

    @PostMapping("/payslip")
    public ResponseEntity<List<PayslipDto>> paySlips(@RequestBody List<EmployeeDto> employees) {
        List<PayslipDto> result = service.generatePayslip(employees);
        return ResponseEntity.ok(result);
    }
}
