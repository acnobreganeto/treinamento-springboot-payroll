package com.empresaxyz.payslip.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.empresaxyz.payslip.controller.dto.EmployeeDto;
import com.empresaxyz.payslip.controller.dto.PayslipDto;
import com.empresaxyz.payslip.util.PayslipCalculus;

@Service
public class PayslipService {
    public List<PayslipDto> generatePayslip(List<EmployeeDto> employees) {
        List<PayslipDto> paySlips = new ArrayList<>();

        for (EmployeeDto employee : employees) {
            PayslipDto payslip = new PayslipDto();
            payslip.setName(employee.getName());
            payslip.setSalary(employee.getSalary());
            payslip.setFgts(PayslipCalculus.fgts(employee.getSalary()));
            payslip.setInss(PayslipCalculus.inss(employee.getSalary()));

            paySlips.add(payslip);
        }

        return paySlips;
    }
}
